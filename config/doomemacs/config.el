;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "monospace" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;;
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

(add-hook 'after-init-hook
          (lambda ()
            (setq default-directory (directory-file-name "~/"))))
;;; UI
(setq-default default-frame-alist (cons '(icon-type . nil) default-frame-alist)
              display-line-numbers-type t
              truncate-lines nil
              treemacs-width 25

              doom-modeline-major-mode-icon t
              doom-modeline-major-mode-color-icon t
              doom-modeline-persp-name t

              company-minimum-prefix-length 1
              company-idle-delay 0.25
              company-box-doc-delay company-idle-delay

              lsp-ui-doc-enable nil
              lsp-ui-peek-enable nil
              lsp-enable-semantic-highlighting t
              lsp-ui-sideline-enable nil
              lsp-ui-sideline-show-diagnostics nil
              lsp-ui-sideline-show-code-actions t
              lsp-ui-imenu-enable t
              lsp-ui-flycheck-enable nil

              lsp-enable-snippet t
              lsp-eldoc-render-all nil
              lsp-signature-render-all t
              lsp-enable-completion-at-point t
              lsp-enable-xref t
              lsp-file-watch-threshold nil

              mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil))
              scroll-step 1
              mouse-wheel-progressive-speed nil
              mouse-wheel-follow-mouse t

              centaur-tabs-set-icons t
              centaur-tabs-cycle-scope 'tabs

              org-bullets-bullet-list '("①" "②" "③" "④" "⑤" "⑥" "⑦" "⑧" "⑨")

              ivy-use-virtual-buffers t)

(set-popup-rules! `((,(rx "*eshell*") :ignore t)
                    (,(rx "*ielm*") :ignore t)))


;;; Keybindings
(map! :leader
      :desc "M-x" "SPC" #'execute-extended-command)

(evil-define-key 'normal key-translation-map (kbd ",") (kbd doom-localleader-key))

(map! (:map ivy-minibuffer-map
        "C-h" #'counsel-up-directory))

(map! :leader
      (:prefix "w"
        :desc "Split vertical" :n "-"
        (lambda! (split-window-vertically)
                 (other-window 1))

        :desc "Split horizontal" :n "/"
        (lambda! (split-window-horizontally)
                 (other-window 1))))

(define-leader-key!
  :desc "Window 0" "0" #'neotree-toggle
  :desc "Window 1" "1" #'winum-select-window-1
  :desc "Window 2" "2" #'winum-select-window-2
  :desc "Window 3" "3" #'winum-select-window-3
  :desc "Window 4" "4" #'winum-select-window-4
  :desc "Window 5" "5" #'winum-select-window-5
  :desc "Window 6" "6" #'winum-select-window-6
  :desc "Window 7" "7" #'winum-select-window-7
  :desc "Window 8" "8" #'winum-select-window-8
  :desc "Window 9" "9" #'winum-select-window-9)



;; these break company box navigation
(define-key evil-insert-state-map (kbd "C-j") nil)
(define-key evil-insert-state-map (kbd "C-k") nil)

; use `//` for comments instead of `/* */`
(add-hook 'c-mode-hook #'(lambda () (c-toggle-comment-style -1)))
