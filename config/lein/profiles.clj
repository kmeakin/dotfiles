{:user {:plugins [[jonase/eastwood "LATEST"]
                  [lein-kibit "LATEST"]
                  [lein-cljfmt "LATEST"]
                  [io.taylorwood/lein-native-image "LATEST"]]
        :native-image {:graal-bin "/usr/lib/jvm/java-8-graal/bin/native-image"}
        :local-repo "/home/karl/.local/.m2/repository"}}
