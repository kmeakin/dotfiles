# cool punctuation:
# ᛟ ᛭ ⇒ ∅ ⋮ ⋯  ⋙ ⋘ Σ Ω Λ λ →

for f in $XDG_CONFIG_HOME/shell/* ;
    source $f
end

# eval (python -m virtualfish compat_aliases auto_activation)

function get_path
    set -l slash (set_color cyan)/(set_color normal)
    set -l tilde (set_color cyan)~(set_color normal)

    dirs -c
    set -l path (dirs)

    # a to replace all instances, q to be quiet
    set path (string replace -a "/" "$slash" "$path")
    set path (string replace -a "~" "$tilde" "$path")
    echo "$path"
end


function fish_prompt
    set -l prev_status $status
    set -l pwd (get_path)

    if test $prev_status -eq 0
        set arrow (set_color green)"~>"(set_color normal)
    else
        set arrow (set_color red)"~>"(set_color normal)
    end


    if test (id -u "$USER") -eq 0
        set glyph (set_color red)Λ(set_color normal)
    else
        set glyph (set_color blue)λ(set_color normal)
    end

    if set -q VIRTUAL_ENV
        echo -n -s (set_color -b blue white) "(" (basename "$VIRTUAL_ENV") ")" (set_color normal) " "
    end

    echo "$glyph $pwd$arrow "
end

function fish_right_prompt
    set -l colon (set_color cyan):(set_color normal)
    set -l time (date +"%H:%M")
#    string replace -a ":" $colon $time
end

