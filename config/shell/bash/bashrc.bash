# -*- mode: shell-script; sh-shell: bash -*-
# vim: set filetype=bash
# shellcheck shell=bash
# Maintainer: Karl Meakin <karlwfmeakin@gmail.com>

# bashrc: run on login and interactive shell init when using bash

# * Prelude
# source shell-agnostic init
source "$XDG_CONFIG_HOME/shell/posix.sh"

# * History
mkdir -p "${XDG_CACHE_HOME:-$HOME/.cache}/bash"
export "HISTFILE=${XDG_CACHE_HOME:-$HOME/.cache}/bash/history"

# * Plugins
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
[ -r /usr/share/doc/pkgfile/command-not-found.bash ] && . /usr/share/doc/pkgfile/command-not-found.bash
