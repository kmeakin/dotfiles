# -*- mode: shell-script; sh-shell: sh -*-
# vim: set filetype=sh
# shellcheck shell=sh
# Maintainer: Karl Meakin <karlwfmeakin@gmail.com>

# posix.sh: run on login and interactive shell init
# could be run by any shell, so must should be POSIX-compatible and
# avoid bashisms

# * Prelude
if hostname | grep -q '.*\.man.*\.ac\.uk'; then
	export IN_KILBURN="true"
fi

# * Setup
# ** functions

list_dedup() {
	# remove duplicate entries in a string with entries seperated by ':' (e.g PATH)
	printf '%s' "$1" | awk -v RS=: -v ORS=: '!x[$0]++' | sed "s/\(.*\).\{1\}/\1/"
}

list_print() {
	# print a string with entries seperated by ':' (e.g PATH) line-by-line
	echo "$1" | tr ':' '\n'
}

list_prepend() {
	# prepend $2 to a string with entries seperated by ':' (e.g PATH)
	echo "${2}${1:+:}${1}"
}

list_append() {
	# prepend $2 to a string with entries seperated by ':' (e.g PATH)
	echo "${1}${1:+:}${2}"
}

# ** Java options
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on \
    -Dswing.aatext=true \
    -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel \
    -Djdk.gtk.version=3"

# ** XDG media dirs

# media dirs
export XDG_MEDIA_DIR="$HOME/usr"
export XDG_MEDIA_SORTED_DIR="$XDG_MEDIA_DIR/media"
export XDG_GAMES_DIR="$XDG_MEDIA_DIR/games"
export STEAM_DIR="$XDG_GAMES_DIR/Steam"

# who even uses these?
export XDG_DESKTOP_DIR="$XDG_MEDIA_DIR/desktop"
export XDG_PUBLICSHARE_DIR="$XDG_MEDIA_DIR/share"
export XDG_TEMPLATES_DIR="$XDG_MEDIA_DIR/templates"

# unsorted media
export XDG_DOWNLOAD_DIR="$XDG_MEDIA_DIR/downloads"
export XDG_TORRENT_DIR="$XDG_MEDIA_DIR/torrents"

# sorted media
export XDG_DOCUMENTS_DIR="$XDG_MEDIA_SORTED_DIR/documents"
export XDG_MUSIC_DIR="$XDG_MEDIA_SORTED_DIR/music"
export XDG_PICTURES_DIR="$XDG_MEDIA_SORTED_DIR/pictures"
export XDG_VIDEOS_DIR="$XDG_MEDIA_SORTED_DIR/videos"

# ** XDG base dirs
export XDG_HOME="$HOME/.local"
export XDG_BIN_HOME="$XDG_HOME/bin"
export XDG_CONFIG_HOME="$XDG_HOME/config"
export XDG_DATA_HOME="$XDG_HOME/share"
export XDG_LIB_HOME="$XDG_HOME/lib"
export XDG_CACHE_HOME="$XDG_HOME/var/cache"
export XDG_LOG_HOME="$XDG_HOME/var/log"
export XDG_RUN_HOME="$XDG_HOME/var/run"

export CARGO_HOME="$XDG_CACHE_HOME/cargo"
export CARGO_INSTALL_ROOT="$XDG_BIN_HOME/cargo"
export DOOMDIR="$XDG_CONFIG_HOME/doomemacs"""
export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export GUILE_HISTORY="$XDG_DATA_HOME/guile/history"
alias guile="guile -l $XDG_CONFIG_HOME/guile/guilerc"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export GTK3_RC_FILES="$XDG_CONFIG_HOME/gtk-3.0/settings.ini"
export GTK_RC_FILES="$XDG_CONFIG_HOME/gtk-1.0/gtkrc"
export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"
export KDEDIR="$XDG_CONFIG_HOME/kde"
export KDEHOME="$XDG_CONFIG_HOME/kde"
export LEIN_HOME="$XDG_CONFIG_HOME/lein"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/passwords"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export ROSWELL_HOME="$XDG_CONFIG_HOME/roswell"
export RUSTUP_HOME="$XDG_CACHE_HOME/rustup"
export SPACEMACSDIR="$XDG_CONFIG_HOME/spacemacs"
# shellcheck disable=SC2016
export VIMINIT='let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc" | source $MYVIMRC'
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export XCOMPOSECACHE="$XDG_CACHE_HOME/Xcompose"
export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
export _JAVA_OPTIONS="$_JAVA_OPTIONS \
    -Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java \
    -Dmaven.repo.local=$XDG_HOME/.m2/repository \
    -Duser.home=$XDG_HOME/"

# TODO: 32bit needed for BFME1?
export WINEPREFIX="$XDG_GAMES_DIR/wineprefixes/wine64"
export WINEARCH=win64

# Some programs annoyingly refuse to use dirs unless they already exist...
mkdir -p "$XDG_DATA_HOME/guile"
mkdir -p "$XDG_DATA_HOME/bpython"

if [ -n "$DISPLAY" ]; then
	xrdb -merge "$XDG_CONFIG_HOME"/X11/Xresources
fi

# ** Keybindings
# map "alt gr" (key right of spacebar) to ctrl, both in Xorg and
# the linux virtual console
if [ -n "$DISPLAY" ]; then
	setxkbmap -option ctrl:ralt_rctrl
else
	:
	# loadkeys "$XDG_DATA_HOME/kbd/keymaps/personal.map"
fi

# ** Set paths
if [ -n "$IN_KILBURN" ]; then
	PATH=$(list_prepend "$PATH" /opt/common/bin)
	PATH=$(list_prepend "$PATH" /opt/teaching/bin)
fi

# add packages installed with `guix` to PATH
if command -v guix >/dev/null; then
	export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
	eval "$(guix package --search-paths=prefix)"
fi

PATH=$(list_prepend "$PATH" "$CARGO_INSTALL_ROOT/bin")
PATH=$(list_prepend "$PATH" "$XDG_DATA_HOME/npm/bin")
PATH=$(list_prepend "$PATH" "$XDG_LIB_HOME/doomemacs/bin")
PATH=$(list_prepend "$PATH" "$XDG_BIN_HOME")
PATH=$(list_prepend "$PATH" "$XDG_BIN_HOME/wrappers")

PATH=$(list_dedup "$PATH")
INFOPATH=$(list_dedup "$INFOPATH")

export PATH INFOPATH

# * Features
# ** Colors
unset LS_COLORS # manchester cs "helpfully" sets their own dircolors
eval "$(dircolors)"

LESS_TERMCAP_mb=$(printf '\e[01;31m')
LESS_TERMCAP_md=$(printf '\e[01;31m')
LESS_TERMCAP_me=$(printf '\e[0m')
LESS_TERMCAP_se=$(printf '\e[0m')
LESS_TERMCAP_so=$(printf '\e[01;44;33m')
LESS_TERMCAP_ue=$(printf '\e[0m')
LESS_TERMCAP_us=$(printf '\e[01;32m')
export LESS_TERMCAP_mb LESS_TERMCAP_md LESS_TERMCAP_me LESS_TERMCAP_se LESS_TERMCAP_so LESS_TERMCAP_ue LESS_TERMCAP_us

alias ls="ls --color=auto"
alias df="dfc" # colorized df output
alias dfc="dfc -t -tmpfs,devtmpfs -Td"
alias diff="diff --color=auto"
alias du="cdu -isrdh"
alias grep="grep --color=auto"
alias less="less -R"

# ** Aliases
# *** Git
alias gadd="git add"
alias gclone="git clone"
alias gcomm="git commit -m"
alias gcommit="gcomm"
alias gdiff="git diff"
alias ginit="git init"
alias glog="git log"
alias gpull="git pull"
alias gpush="git push --follow-tags"
alias gstat="git status"
alias gstatus="gstat"

# *** ls
alias ls="ls --color=auto --group-directories-first"
alias ls.="ls -d .*"
alias l.="ls -ld .*"
alias ll.="ls. -l"

alias la="ls -A"
alias lla="ls -lA"
alias lal="lla"
alias l="ls -l"
alias ll="ls -l"

alias sl="ls"
alias sla="la"

# *** Package managers
test -f /etc/os-release && . /etc/os-release
DISTRO=$NAME
# shellcheck disable=SC2139
case "$DISTRO" in
"Arch Linux" | "Antergos Linux")
	AUR="pikaur"
	alias 'pkg-install'="$AUR -S"
	alias 'pkg-info'="$AUR -Qi"
	alias 'pkg-update'="$AUR -Syu"
	alias 'pkg-remove'="$AUR -Rs"
	alias 'pkg-list'="$AUR -Qet"
	;;
"NixOS")
	alias 'pkg-install'='nix-env -iA'
	alias 'pkg-update'='nix-channel --update && nixos-rebuild switch'
	alias 'pkg-remove'='nix-env --uninstall'
	;;
esac

alias 'pkgI'='pkg-info'
alias 'pkgi'='pkg-install'
alias 'pkgl'='pkg-list'
alias 'pkgr'='pkg-remove'
alias 'pkgu'='pkg-update'

# *** (Neo)vi(m)
# use the best available version of that 1970's editor other than emacs
if command which --skip-alias nvim >/dev/null 2>/dev/null; then
	_editor="nvim"
elif command which --skip-alias vim >/dev/null 2>/dev/null; then
	_editor="vim"
else
	_editor="vi"
fi
export EDITOR="$_editor"
export VISUAL="$_editor"

# shellcheck disable=SC2139
alias nvim="$EDITOR"
alias nvimdiff="$EDITOR -d"
# shellcheck disable=SC2139
alias vim="$EDITOR"
alias vimdiff="$EDITOR -d"
# shellcheck disable=SC2139
alias vi="$EDITOR"
unset _editor

# *** SSH
eval $(gpg-agent --daemon 2>/dev/null)

# Misc
alias chmox="chmod +x"
alias mdkir="mkdir -p"
alias rmdir="rmdir -p"
alias update-grub="grub-mkconfig -o /boot/grub/grub.cfg"
alias xclip="xclip -sel clip" # copy into main clipboard rather than mouse clipboard
alias ffprobe="ffprobe -hide_banner"
alias pingtest="ping 8.8.8.8"
alias ps-tree="ps axjf"
alias sudo='sudo -E '

# * Functions
environ() {
	# print $1's environment variables
	tr '\000' '\n' <"/proc/$1/environ"
}

kbd_light() {
	dbus-send --system --type=method_call \
		--dest="org.freedesktop.UPower" \
		"/org/freedesktop/UPower/KbdBacklight" \
		"org.freedesktop.UPower.KbdBacklight.SetBrightness" \
		"int32:$1"
}
