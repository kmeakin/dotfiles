import atexit
import os
import readline
import xdg.BaseDirectory

python_cache = os.path.join(xdg.BaseDirectory.xdg_data_home, "python")
os.makedirs(python_cache, exist_ok=True)

histfile = os.path.join(python_cache, "history")
try:
    readline.read_history_file(histfile)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
