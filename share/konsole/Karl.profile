[Appearance]
ColorScheme=custom-color-scheme-1
Font=Liberation Mono,11,-1,5,50,0,0,0,0,0,Regular

[Cursor Options]
CursorShape=1

[General]
Name=Karl
Parent=FALLBACK/
ShowTerminalSizeHint=false

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
