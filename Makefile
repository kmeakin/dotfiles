SHELL := /bin/bash
# -B flag forces make to unconditionally build targets every time
FLAGS=-B

export XDG_HOME="${HOME}/.local"
export XDG_BIN_HOME="${XDG_HOME}/bin"
export XDG_CONFIG_HOME="${XDG_HOME}/config"
export XDG_DATA_HOME="${XDG_HOME}/share"
export XDG_LIB_HOME="${XDG_HOME}/lib"
export XDG_CACHE_HOME="${XDG_HOME}/var/cache"
export XDG_LOG_HOME="${XDG_HOME}/var/log"
export XDG_RUN_HOME="${XDG_HOME}/var/run"

STOW_CMD:=$(shell command -v stow)
ifeq ($(STOW_CMD),)
STOW_CMD:=./stow/stow
export PERL5LIB=$(shell pwd)/stow
export PERLLIB=$(shell pwd)/stow
endif
STOW_FLAGS=--verbose=0

# hack to run with FLAGS everytime
# https://stackoverflow.com/questions/10567890/parallel-make-set-j8-as-the-default-option
all:; @$(MAKE) _all $(FLAGS)
_all: config share bin

config: Makefile
	mkdir -p ${XDG_CONFIG_HOME}
	$(STOW_CMD) $(STOW_FLAGS) --target $(XDG_CONFIG_HOME) config
	ln -sf $(XDG_CONFIG_HOME)/shell/posix.sh $(HOME)/.profile
	ln -sf $(XDG_CONFIG_HOME)/shell/bash/bashrc.bash $(HOME)/.bashrc
	ln -sf $(XDG_CONFIG_HOME)/shell/zsh/zshrc.zsh $(HOME)/.zshrc
	ln -sf $(HOME)/usr $(XDG_HOME)/usr

share: Makefile
	mkdir -p $(XDG_DATA_HOME)
	$(STOW_CMD) $(STOW_FLAGS) --target $(XDG_DATA_HOME) share

bin: Makefile
	mkdir -p $(XDG_BIN_HOME)
	$(STOW_CMD) $(STOW_FLAGS) --target $(XDG_BIN_HOME) bin
